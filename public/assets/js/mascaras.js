jQuery(function($){
   $(".data").mask("99/99/9999");
   $(".telefone").mask("(99) 9999-9999");
   $(".cpf").mask("999.999.999-99");
   $(".placa").mask("aaa-9999");
   $(".ano").mask("9999");
});

$('form[data-confirm]').submit(function() {
    if(!confirm($(this).attr('data-confirm'))) {
        return false;
    }
});