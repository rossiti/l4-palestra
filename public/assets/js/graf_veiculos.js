var options = {
    responsive:true, 

};

var disponiveis = $('#disponiveis').val().trim();
var alugados = $('#alugados').val().trim();

var data = {
        labels: ["Disponíveis X Alugados"],
        datasets: [
            {
                label: "Veículos Disponíveis",
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor: "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data: [disponiveis]
            },
            {
                label: "Veículos Alugados",
                fillColor: "rgba(151,187,205,0.5)",
                strokeColor: "rgba(151,187,205,0.8)",
                highlightFill: "rgba(151,187,205,0.75)",
                highlightStroke: "rgba(151,187,205,1)",
                data: [alugados]
            }
        ]
    }; 

window.onload = function(){
    var ctx = document.getElementById("GraficoBarra").getContext("2d");
    var BarChart = new Chart(ctx).Bar(data, options);
}