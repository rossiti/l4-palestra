<?php

class Veiculo extends BaseModel
{
	protected $table = 'veiculos';

    protected $fillable = array('modelo', 'placa', 'cor', 'ano', 'valor', 'marca_id', 'created_at', 'updated_at');

	public static $rules = array(
        'modelo' => 'required|min:3|max:50',
        'placa' => 'required|min:3|max:8',
        'cor' => 'required|min:3|max:50',
        'ano' => 'required|min:4|max:4',
        'valor' => 'required',
    );

    public function marca()
    {
        return $this->belongsTo('Marca', 'marca_id');
    }

    public function reserva()
    {
        return $this->hasMany('Reserva', 'veiculo_id');
    }

    public static function options()
    {
        // Listo todos os id's dos veículos alugados
        $reservas = DB::table('reservas')->lists('veiculo_id');

        // Listo todos veículos que não estão alugados
        $result = static::whereNotIn('id', $reservas)
                            ->orderBy('modelo')
                            ->lists('modelo', 'id');

        return array('' => 'Selecione uma opção') + $result;
    }

}