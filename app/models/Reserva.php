<?php

class Reserva extends BaseModel
{
	protected $table = 'reservas';

    protected $fillable = array('data', 'cliente_id', 'veiculo_id', 'created_at', 'updated_at');

	public static $rules = array();

    public function cliente()
    {
        return $this->belongsTo('Cliente', 'cliente_id');
    }

    public function veiculo()
    {
        return $this->belongsTo('Veiculo', 'veiculo_id');
    }

    public static function disponiveis()
    {
        // Listo todos os id's dos veículos alugados
        $reservas = DB::table('reservas')->lists('veiculo_id');

        // Listo todos veículos que não estão alugados
        $disponiveis = DB::table('veiculos')
                            ->whereNotIn('id', $reservas)
                            ->count();
        return $disponiveis;
    }

    public static function alugados()
    {
        // Listo todos os id's dos veículos alugados
        $reservas = DB::table('reservas')->lists('veiculo_id');

        // Listo todos veículos que estão alugados
        $alugados = DB::table('veiculos')
                            ->whereIn('id', $reservas)
                            ->count();
        return $alugados;
    }

}