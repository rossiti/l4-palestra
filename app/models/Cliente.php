<?php

class Cliente extends BaseModel
{
	protected $table = 'clientes';

    protected $fillable = array('nome', 'cpf', 'telefone', 'email','created_at', 'updated_at');

	public static $rules = array(
        'nome' => 'required|min:3|max:50',
        'cpf' => 'required',
        'telefone' => 'required',
        'email' => 'required',
    );

    public static function options()
    {
        $result = static::orderBy('nome')->lists('nome', 'id');

        return array('' => 'Selecione uma opção') + $result;
    }

    public function reserva()
    {
        return $this->hasMany('Reserva', 'cliente_id');
    }

}