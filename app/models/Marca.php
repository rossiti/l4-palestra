<?php

class Marca extends BaseModel
{
	protected $table = 'marcas';

    protected $fillable = array('marca', 'created_at', 'updated_at');

	public static $rules = array(
        'marca' => 'required|min:3|max:50',
    );

    public function veiculo()
    {
        return $this->hasMany('Veiculo', 'marca_id');
    }

    public static function options()
    {
        $result = static::orderBy('marca')->lists('marca', 'id');

        return array('' => 'Selecione uma opção') + $result;
    }

}