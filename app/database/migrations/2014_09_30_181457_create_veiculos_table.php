<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVeiculosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('veiculos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('modelo', 50);
			$table->string('placa', 8);
			$table->string('cor', 50);
			$table->string('ano', 4);
			$table->decimal('valor', 9, 2);

			$table->integer('marca_id')->unsigned();
			$table->foreign('marca_id')->references('id')->on('marcas');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('veiculos');
	}

}
