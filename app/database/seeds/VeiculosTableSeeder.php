<?php

class VeiculosTableSeeder extends Seeder {

    public function run()
    {
        DB::table('veiculos')->delete();

        Veiculo::create(array(
        	'modelo'    => 'Golf',
            'placa'     => 'NYX-0199',
            'cor'       => 'BRANCA',
            'ano'       => '2014',
            'valor'     => '44999.00',
            'marca_id'  => '1' 
        ));
    }

}