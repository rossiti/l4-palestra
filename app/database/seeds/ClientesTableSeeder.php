<?php

class ClientesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('clientes')->delete();

        Cliente::create(array(
        	'nome'    	=> 'Melina Oliveira',
            'cpf'     	=> '322.901.234-90',
            'telefone'  => '(17) 3631-9999',
            'email'		=> 'melina@gmail.com'
        ));

        Cliente::create(array(
            'nome'      => 'Joaquim Soares',
            'cpf'       => '784.093.163-32',
            'telefone'  => '(17) 3631-0155',
            'email'     => 'joaquim@gmail.com'
        ));

        Cliente::create(array(
            'nome'      => 'Leticia Vieira',
            'cpf'       => '322.901.234-90',
            'telefone'  => '(17) 3641-0322',
            'email'     => 'leticia@gmail.com'
        ));
    }

}