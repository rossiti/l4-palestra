<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// Descomente as linhas abaixo para popular o banco de dados
		// $this->call('MarcasTableSeeder');
		// $this->call('VeiculosTableSeeder');
		// $this->call('ClientesTableSeeder');
		// $this->call('ReservasTableSeeder');
	}

}
