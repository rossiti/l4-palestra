<?php

class ReservasTableSeeder extends Seeder {

    public function run()
    {
        DB::table('reservas')->delete();

        Reserva::create(array(
        	'cliente_id'    => 1,
            'veiculo_id'     => 1
        ));
    }

}