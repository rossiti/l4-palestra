<?php

class MarcasTableSeeder extends Seeder {

    public function run()
    {
        DB::table('marcas')->delete();

        Marca::create(array(
        	'marca' => 'Volkswagen',
        ));

        Marca::create(array(
        	'marca' => 'Ford',
        ));

        Marca::create(array(
        	'marca' => 'GM/Chevrolet',
        ));

        Marca::create(array(
        	'marca' => 'Fiat',
        ));
    }

}