<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::resource('marca', 'MarcaController');
Route::resource('cliente', 'ClienteController');
Route::resource('veiculo', 'VeiculoController');
Route::resource('reserva', 'ReservaController');

Route::get('teste', function()
{
	return View::make('teste');
});

Route::get('/', function()
{
	return View::make('home.index');
});
