@extends('layouts.base')

@section('content')
	<h4>
		<span class="glyphicon glyphicon-list-alt"></span> Listagem de clientes
		<a href="{{ URL::to('cliente/create') }}" class="btn btn-success navbar-right"><span class="glyphicon glyphicon-plus-sign"></span> Novo</a>
	</h4>
	<hr>
	{{ Form::open(array('url' => 'cliente', 'method' => 'get', 'class' => 'form-inline', 'role' => 'form')) }}
		<div class="form-group">
			{{ Form::text('nome', $nome, array('placeholder' => 'Nome', 'class' => 'form-control')) }}
		</div>
		{{ Form::button('<span class="glyphicon glyphicon-search"></span> Pesquisar', array('type' => 'submit', 'class' => 'btn btn-default')) }}
	{{ Form::close() }}
	<hr>
	@if($clientes->getItems())
		Exibindo de {{ $clientes->getFrom() }} até {{ $clientes->getTo() }} de {{ $clientes->getTotal() }} registros.
		<hr>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th><a href="{{ URL::to('cliente?sort=nome' . $str) }}">Nome</a></th>
					<th><a href="{{ URL::to('cliente?sort=cpf' . $str) }}">CPF</a></th>
					<th colspan="3"></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($clientes as $cliente)
					<tr>
						<td>{{ e($cliente->nome) }}</td>

						<td>{{ $cliente->cpf }}</td>
						<td>{{ $cliente->telefone }}</td>
						<td>{{ $cliente->email }}</td>

						<td class="action">{{ link_to('cliente/' . $cliente->id, 'Detalhar', array('class' => 'btn btn-info btn-sm', 'title' => 'Detalhar')) }}</td>

						<td class="action">{{ link_to('cliente/' . $cliente->id . '/edit', 'Editar', array('class' => 'btn btn-primary btn-sm', 'title' => 'Editar')) }}</td>

						<td class="action">
							{{ Form::open(array('url' => 'cliente/' . $cliente->id, 'method' => 'delete', 'data-confirm' => 'Deseja realmente excluir o registro selecionado?')) }}
								{{ Form::button('Apagar', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'title' => 'Apagar')) }}
							{{ Form::close() }}
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		{{ $pagination }}
	@else
		<p class="text-danger"><strong>{{ Lang::get('messages.MSG011') }}</strong></p>
	@endif
@stop