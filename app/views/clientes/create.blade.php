@extends('layouts.base')

@section('content')
	<h4>
		<span class="glyphicon glyphicon-plus-sign"></span> Adicionar nova cliente
		<a href="{{ URL::to('cliente') }}" class="btn btn-info navbar-right"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a>
	</h4>
	<hr>
	{{ Form::open(array('url' => 'cliente', 'class' => 'form-horizontal row', 'role' => 'form')) }}

		<div class="col-xs-6 {{ $errors->first('nome') ? 'has-error' : '' }}">
			{{ Form::label('nome', '* Nome', array('class' => 'control-label')) }}
        	{{ Form::text('nome', Input::old('nome'), array('class' => 'form-control')) }}
        	{{ $errors->first('nome', '<span class="text-danger">:message</span>') }}
		</div>

		<div class="col-xs-6 {{ $errors->first('cpf') ? 'has-error' : '' }}">
			{{ Form::label('cpf', '* CPF', array('class' => 'control-label')) }}
        	{{ Form::text('cpf', Input::old('cpf'), array('class' => 'form-control cpf')) }}
        	{{ $errors->first('cpf', '<span class="text-danger">:message</span>') }}
		</div>

		<div class="col-xs-6 {{ $errors->first('telefone') ? 'has-error' : '' }}">
			{{ Form::label('telefone', '* Telefone', array('class' => 'control-label')) }}
        	{{ Form::text('telefone', Input::old('telefone'), array('class' => 'form-control telefone')) }}
        	{{ $errors->first('telefone', '<span class="text-danger">:message</span>') }}
		</div>

		<div class="col-xs-6 {{ $errors->first('email') ? 'has-error' : '' }}">
			{{ Form::label('email', '* E-mail', array('class' => 'control-label')) }}
        	{{ Form::text('email', Input::old('email'), array('class' => 'form-control')) }}
        	{{ $errors->first('email', '<span class="text-danger">:message</span>') }}
		</div>

		<div class="col-btn">
			{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
		</div>

	{{ Form::close() }}
@stop