@extends('layouts.base')

@section('content')
	<h4>
		<span class="glyphicon glyphicon-plus-sign"></span> Alugar veículo
		<a href="{{ URL::to('reserva') }}" class="btn btn-info navbar-right"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a>
	</h4>
	<hr>
	{{ Form::open(array('url' => 'reserva', 'class' => 'form-horizontal row', 'role' => 'form')) }}
		
		<div class="col-xs-6 {{ $errors->first('cliente_id') ? 'has-error' : '' }}">
			{{ Form::label('cliente_id', '* Cliente', array('class' => 'control-label')) }}
        	{{ Form::select('cliente_id', Cliente::options(), Input::old('cliente_id'), array('class' => 'form-control select2')) }}
        	{{ $errors->first('cliente_id', '<span class="text-danger">:message</span>') }}
		</div>

		<div class="col-xs-6 {{ $errors->first('veiculo_id') ? 'has-error' : '' }}">
			{{ Form::label('veiculo_id', '* Veículo', array('class' => 'control-label')) }}
        	{{ Form::select('veiculo_id', Veiculo::options(), Input::old('veiculo_id'), array('class' => 'form-control select2')) }}
        	{{ $errors->first('veiculo_id', '<span class="text-danger">:message</span>') }}
		</div>

		<div class="col-btn">
			{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
		</div>

	{{ Form::close() }}
@stop