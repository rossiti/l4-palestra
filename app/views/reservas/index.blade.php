@extends('layouts.base')

@section('content')
	<h4>
		<span class="glyphicon glyphicon-list-alt"></span> Listagem de reservas
		<a href="{{ URL::to('reserva/create') }}" class="btn btn-success navbar-right"><span class="glyphicon glyphicon-plus-sign"></span> Alugar Veículo</a>
	</h4>
	<hr>
	{{ Form::open(array('url' => 'reserva', 'method' => 'get', 'class' => 'form-inline', 'role' => 'form')) }}
		<div class="form-group">
			{{ Form::text('nome', $nome, array('placeholder' => 'Nome', 'class' => 'form-control')) }}
		</div>

		<div class="form-group">
			{{ Form::text('placa', $placa, array('placeholder' => 'Placa', 'class' => 'form-control')) }}
		</div>

		{{ Form::button('<span class="glyphicon glyphicon-search"></span> Pesquisar', array('type' => 'submit', 'class' => 'btn btn-default')) }}
	{{ Form::close() }}
	<hr>
	@if($reservas->getItems())
		Exibindo de {{ $reservas->getFrom() }} até {{ $reservas->getTo() }} de {{ $reservas->getTotal() }} registros.
		<hr>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th><a href="{{ URL::to('reserva?sort=nome' . $str) }}">Cliente</a></th>
					<th><a href="{{ URL::to('reserva?sort=modelo' . $str) }}">Veículo</a></th>
					<th><a href="{{ URL::to('reserva?sort=placa' . $str) }}">Placa</a></th>
					<th><a href="{{ URL::to('reserva?sort=reservas.created_at' . $str) }}">Data da Locação</a></th>
					<th colspan="3"></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($reservas as $reserva)
					<tr>
						<td>{{ $reserva->cliente->nome }}</td>
						<td>{{ $reserva->veiculo->modelo }}</td>
						<td>{{ $reserva->veiculo->placa }}</td>
						<td>{{ Util::toTimestamps($reserva->created_at) }}</td>

						<td class="action">
							{{ Form::open(array('url' => 'reserva/' . $reserva->id, 'method' => 'delete', 'data-confirm' => 'Deseja realmente excluir o registro selecionado?')) }}
								{{ Form::button('Devolução', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'title' => 'Devolução')) }}
							{{ Form::close() }}
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		{{ $pagination }}
	@else
		<p class="text-danger"><strong>{{ Lang::get('messages.MSG011') }}</strong></p>
	@endif
@stop