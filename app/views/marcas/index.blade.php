@extends('layouts.base')

@section('content')
	<h4>
		<span class="glyphicon glyphicon-list-alt"></span> Listagem de marcas
		<a href="{{ URL::to('marca/create') }}" class="btn btn-success navbar-right"><span class="glyphicon glyphicon-plus-sign"></span> Novo</a>
	</h4>
	<hr>
	{{ Form::open(array('url' => 'marca', 'method' => 'get', 'class' => 'form-inline', 'role' => 'form')) }}
		<div class="form-group">
			{{ Form::text('marca', $marca, array('placeholder' => 'Marca', 'class' => 'form-control')) }}
		</div>
		{{ Form::button('<span class="glyphicon glyphicon-search"></span> Pesquisar', array('type' => 'submit', 'class' => 'btn btn-default')) }}
	{{ Form::close() }}
	<hr>
	@if($marcas->getItems())
		Exibindo de {{ $marcas->getFrom() }} até {{ $marcas->getTo() }} de {{ $marcas->getTotal() }} registros.
		<hr>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th><a href="{{ URL::to('marca?sort=marca' . $str) }}">Marca</a></th>
					<th colspan="3"></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($marcas as $marca)
					<tr>
						<td>{{ e($marca->marca) }}</td>

						<td class="action">{{ link_to('marca/' . $marca->id, 'Detalhar', array('class' => 'btn btn-info btn-sm', 'title' => 'Detalhar')) }}</td>

						<td class="action">{{ link_to('marca/' . $marca->id . '/edit', 'Editar', array('class' => 'btn btn-primary btn-sm', 'title' => 'Editar')) }}</td>

						<td class="action">
							{{ Form::open(array('url' => 'marca/' . $marca->id, 'method' => 'delete', 'data-confirm' => 'Deseja realmente excluir o registro selecionado?')) }}
								{{ Form::button('Apagar', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'title' => 'Apagar')) }}
							{{ Form::close() }}
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		{{ $pagination }}
	@else
		<p class="text-danger"><strong>{{ Lang::get('messages.MSG011') }}</strong></p>
	@endif
@stop