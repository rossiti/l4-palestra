@extends('layouts.base')

@section('content')
	<h4>
		<span class="glyphicon glyphicon-info-sign"></span> Detalhes da marca
		<a href="{{ URL::to('marca') }}" class="btn btn-info navbar-right"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a>
	</h4>
	<hr>
	<div class="jumbotron">
        <center><h2>{{ $marca->marca}}</h2></center>
        <p><strong>Criado em: </strong> {{ Util::toTimestamps($marca->created_at) }}</p>
        <p><strong>Última atualização: </strong> {{ Util::toTimestamps($marca->updated_at) }}</p>

    </div>
@stop