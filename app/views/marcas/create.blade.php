@extends('layouts.base')

@section('content')
	<h4>
		<span class="glyphicon glyphicon-plus-sign"></span> Adicionar nova marca
		<a href="{{ URL::to('marca') }}" class="btn btn-info navbar-right"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a>
	</h4>
	<hr>
	{{ Form::open(array('url' => 'marca', 'class' => 'form-horizontal row', 'role' => 'form')) }}

		<div class="col-xs-6 {{ $errors->first('marca') ? 'has-error' : '' }}">
			{{ Form::label('marca', '* Marca', array('class' => 'control-label')) }}
        	{{ Form::text('marca', Input::old('marca'), array('class' => 'form-control')) }}
        	{{ $errors->first('marca', '<span class="text-danger">:message</span>') }}
		</div>

		<div class="col-btn">
			{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
		</div>

	{{ Form::close() }}
@stop