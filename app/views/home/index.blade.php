@extends('layouts.base')

@section('content')
    
    <div>
    	<input type="hidden" id="disponiveis" value="{{ Reserva::disponiveis() }}" />
    	<input type="hidden" id="alugados" value="{{ Reserva::alugados() }}" />
    	@include('dashboard.graf_veiculos')
    </div>
   
@stop
