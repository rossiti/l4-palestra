@extends('layouts.base')

@section('content')
	<h4>
		<span class="glyphicon glyphicon-plus-sign"></span> Adicionar novo veículo
		<a href="{{ URL::to('veiculo') }}" class="btn btn-info navbar-right"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a>
	</h4>
	<hr>
	{{ Form::open(array('url' => 'veiculo', 'class' => 'form-horizontal row', 'role' => 'form')) }}

		<div class="col-xs-6 {{ $errors->first('modelo') ? 'has-error' : '' }}">
			{{ Form::label('modelo', '* Modelo', array('class' => 'control-label')) }}
        	{{ Form::text('modelo', Input::old('modelo'), array('class' => 'form-control')) }}
        	{{ $errors->first('modelo', '<span class="text-danger">:message</span>') }}
		</div>

		<div class="col-xs-6 {{ $errors->first('placa') ? 'has-error' : '' }}">
			{{ Form::label('placa', '* Placa', array('class' => 'control-label')) }}
        	{{ Form::text('placa', Input::old('placa'), array('class' => 'form-control placa')) }}
        	{{ $errors->first('placa', '<span class="text-danger">:message</span>') }}
		</div>

		<div class="col-xs-6 {{ $errors->first('cor') ? 'has-error' : '' }}">
			{{ Form::label('cor', '* Cor', array('class' => 'control-label')) }}
        	{{ Form::text('cor', Input::old('cor'), array('class' => 'form-control')) }}
        	{{ $errors->first('cor', '<span class="text-danger">:message</span>') }}
		</div>

		<div class="col-xs-6 {{ $errors->first('ano') ? 'has-error' : '' }}">
			{{ Form::label('ano', '* Ano', array('class' => 'control-label')) }}
        	{{ Form::text('ano', Input::old('ano'), array('class' => 'form-control ano')) }}
        	{{ $errors->first('ano', '<span class="text-danger">:message</span>') }}
		</div>

		<div class="col-xs-6 {{ $errors->first('valor') ? 'has-error' : '' }}">
			{{ Form::label('valor', '* Valor', array('class' => 'control-label')) }}
			<div class="input-group">
  			<span class="input-group-addon">R$</span>
        		{{ Form::text('valor', Input::old('valor'), array('class' => 'form-control')) }}
        	</div>
        	{{ $errors->first('valor', '<span class="text-danger">:message</span>') }}
		</div>

		<div class="col-xs-6 {{ $errors->first('marca_id') ? 'has-error' : '' }}">
			{{ Form::label('marca_id', '* Marca', array('class' => 'control-label')) }}
        	{{ Form::select('marca_id', Marca::options(), Input::old('marca_id'), array('class' => 'form-control select2')) }}
        	{{ $errors->first('marca_id', '<span class="text-danger">:message</span>') }}
		</div>

		<div class="col-btn">
			{{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
		</div>

	{{ Form::close() }}
@stop