@extends('layouts.base')

@section('content')
	<h4>
		<span class="glyphicon glyphicon-info-sign"></span> Detalhes do veículo
		<a href="{{ URL::to('veiculo') }}" class="btn btn-info navbar-right"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a>
	</h4>
	<hr>
	<div class="jumbotron">
        <center><h2>{{ $veiculo->modelo}}</h2></center>

		<p><strong>Placa: </strong> {{$veiculo->placa}}</p>
		<p><strong>Cor: </strong> {{$veiculo->cor}}</p>
		<p><strong>Ano: </strong> {{$veiculo->ano}}</p>
		<p><strong>Valor: </strong> R$ {{$veiculo->valor}}</p>
		<p><strong>Marca: </strong> {{$veiculo->marca->marca}}</p>

        <p><strong>Criado em: </strong> {{ Util::toTimestamps($veiculo->created_at) }}</p>
        <p><strong>Última atualização: </strong> {{ Util::toTimestamps($veiculo->updated_at) }}</p>

    </div>
@stop