@extends('layouts.base')

@section('content')
	<h4>
		<span class="glyphicon glyphicon-list-alt"></span> Listagem de veículos
		<a href="{{ URL::to('veiculo/create') }}" class="btn btn-success navbar-right"><span class="glyphicon glyphicon-plus-sign"></span> Novo</a>
	</h4>
	<hr>
	{{ Form::open(array('url' => 'veiculo', 'method' => 'get', 'class' => 'form-inline', 'role' => 'form')) }}
		<div class="form-group">
			{{ Form::text('modelo', $modelo, array('placeholder' => 'Modelo', 'class' => 'form-control')) }}
		</div>

		<div class="form-group">
			{{ Form::text('placa', $placa, array('placeholder' => 'Placa', 'class' => 'form-control placa')) }}
		</div>

		{{ Form::button('<span class="glyphicon glyphicon-search"></span> Pesquisar', array('type' => 'submit', 'class' => 'btn btn-default')) }}
	{{ Form::close() }}
	<hr>
	@if($veiculos->getItems())
		Exibindo de {{ $veiculos->getFrom() }} até {{ $veiculos->getTo() }} de {{ $veiculos->getTotal() }} registros.
		<hr>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th><a href="{{ URL::to('veiculo?sort=modelo' . $str) }}">Modelo</a></th>
					<th><a href="{{ URL::to('veiculo?sort=placa' . $str) }}">Placa</a></th>
					<th><a href="{{ URL::to('veiculo?sort=cor' . $str) }}">Cor</a></th>
					<th><a href="{{ URL::to('veiculo?sort=ano' . $str) }}">Ano</a></th>
					<th><a href="{{ URL::to('veiculo?sort=valor' . $str) }}">Valor</a></th>
					<th><a href="{{ URL::to('veiculo?sort=marca' . $str) }}">Marca</a></th>
					<th colspan="3"></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($veiculos as $veiculo)
					<tr>
						<td>{{ e($veiculo->modelo) }}</td>
						<td>{{ e($veiculo->placa) }}</td>
						<td>{{ e($veiculo->cor) }}</td>
						<td>{{ e($veiculo->ano) }}</td>
						<td>{{ e($veiculo->valor) }}</td>
						<td>{{ e($veiculo->marca->marca) }}</td>

						<td class="action">{{ link_to('veiculo/' . $veiculo->id, 'Detalhar', array('class' => 'btn btn-info btn-sm', 'title' => 'Detalhar')) }}</td>

						<td class="action">{{ link_to('veiculo/' . $veiculo->id . '/edit', 'Editar', array('class' => 'btn btn-primary btn-sm', 'title' => 'Editar')) }}</td>

						<td class="action">
							{{ Form::open(array('url' => 'veiculo/' . $veiculo->id, 'method' => 'delete', 'data-confirm' => 'Deseja realmente excluir o registro selecionado?')) }}
								{{ Form::button('Apagar', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'title' => 'Apagar')) }}
							{{ Form::close() }}
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		{{ $pagination }}
	@else
		<p class="text-danger"><strong>{{ Lang::get('messages.MSG011') }}</strong></p>
	@endif
@stop