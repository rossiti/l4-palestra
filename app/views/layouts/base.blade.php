<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Locação de Veículos</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap e plugins necessários-->
		{{ HTML::style('assets/css/bootstrap.min.css') }}
		{{ HTML::style('assets/css/select2.css') }}
		{{ HTML::style('assets/css/select2-bootstrap.css') }}

		<!-- CSS  -->
		{{ HTML::style('assets/css/base.css') }}

		
			
	</head>
	<body>
		<div class="navbar navbar-default navbar-fixed-top">	      		
      		<div class="container">
	        	@include('partials.menu')
      		</div>
    	</div>
    	<div class="container">
    		{{ HTML::flash_message() }}
    		@yield('content')
    	</div>
	</body>
	
	<!-- jQuery, Bootstrap e plugins necessários -->
	{{ HTML::script('assets/js/jquery.min.js', array('defer' => 'defer')) }}
	{{ HTML::script('assets/js/bootstrap.min.js', array('defer' => 'defer')) }}		
	{{ HTML::script('assets/js/jquery.maskedinput.min.js', array('defer' => 'defer')) }}
	{{ HTML::script('assets/js/select2.min.js', array('defer' => 'defer')) }}
	{{ HTML::script('assets/js/select2_locale_pt-BR.js', array('defer' => 'defer')) }}
	{{ HTML::script('assets/js/Chart.js', array('defer' => 'defer')) }}

	<!-- JS -->
	{{ HTML::script('assets/js/mascaras.js', array('defer' => 'defer')) }}
	{{ HTML::script('assets/js/base.js', array('defer' => 'defer')) }}
	{{ HTML::script('assets/js/graf_veiculos.js', array('defer' => 'defer')) }}
</html>