<div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="/">Rent a Car</a>
	
</div>
<div class="collapse navbar-collapse">  
	<ul class="nav navbar-nav">
        <li>{{ link_to('cliente', 'Clientes') }}</li>
        <li>{{ link_to('marca', 'Marcas') }}</li>
        <li>{{ link_to('veiculo', 'Veículos') }}</li>        
        <li>{{ link_to('reserva', 'Reservas') }}</li>     
	</ul>
</div>
