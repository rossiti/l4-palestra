<?php

class VeiculoController extends \BaseController {

	protected $veiculo;

    public function __construct(Veiculo $veiculo)
    {
        parent::__construct();
        $this->veiculo = $veiculo;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$modelo = $placa = null;

        $fields = array('modelo');
        $sort = in_array(Input::get('sort'), $fields) ? Input::get('sort') : 'modelo';
        $order = Input::get('order') === 'desc' ? 'desc' : 'asc';

        $veiculos = $this->veiculo->orderBy($sort, $order);

        if(Input::has('modelo')) {
            $veiculos = $veiculos->where('modelo', 'LIKE', "%". Input::get('modelo') ."%");
            $veiculo = '&modelo='. Input::get('modelo');
        }

        if(Input::has('placa')) {
            $veiculos = $veiculos->where('placa', 'LIKE', "%". Input::get('placa') ."%");
            $veiculo = '&placa='. Input::get('placa');
        }

        $veiculos = $veiculos->paginate(15);

        $pagination = $veiculos->appends(array(
            'modelo' => Input::get('modelo'),
            'placa' => Input::get('placa'),
            'sort' => Input::get('sort'),
            'order' => Input::get('order')
        ))->links();

        return View::make('veiculos.index')
            ->with(array(
                'modelo' => Input::get('modelo'),
                'placa' => Input::get('placa'),
                'veiculos' => $veiculos,
                'pagination' => $pagination,
                'str' => '&order='.(Input::get('order') == 'asc' || null ? 'desc' : 'asc') . $modelo . $placa
        ));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('veiculos.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
        $validator = $this->veiculo->validate($input);

        if($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator)
                ->with('danger', Lang::get('messages.MSG001'));
        } else {
            $this->veiculo->create($input);

            return Redirect::to('veiculo')
                ->with('success', Lang::get('messages.MSG002'));
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try {
            $veiculo = $this->veiculo->findOrFail($id);
            return View::make('veiculos.show', compact('veiculo'));
        } catch(ModelNotFoundException $e) {
            return Redirect::to('veiculo')
                ->with('danger', Lang::get('messages.MSG003'));
        }
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try {
            $veiculo = $this->veiculo->findOrFail($id);
            return View::make('veiculos.edit', compact('veiculo'));
        } catch(ModelNotFoundException $e) {
            return Redirect::to('veiculo')
                ->with('danger', Lang::get('messages.MSG003'));
        }
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
        $validator = $this->veiculo->validate($input);

        if($validator->fails()){
            return Redirect::back()
                ->withInput()
                ->withErrors($validator)
                ->with('danger', Lang::get('messages.MSG004'));
        } else {
            $this->veiculo->find($id)->update($input);

            return Redirect::to('veiculo')
                ->with('success', Lang::get('messages.MSG005'));
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try {
            $this->veiculo->find($id)->delete();
            return Redirect::to('veiculo')
                ->with('success', Lang::get('messages.MSG006'));
        } catch (Exception $e) {
            return Redirect::to('veiculo')
                ->with('warning', Lang::get('messages.MSG007'));
        }	
	}


}
