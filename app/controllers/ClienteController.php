<?php

class ClienteController extends \BaseController {

	protected $cliente;

    public function __construct(Cliente $cliente)
    {
        parent::__construct();
        $this->cliente = $cliente;
    }


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$nome = null;

        $fields = array('nome', 'cpf', 'telefone', 'email');
        $sort = in_array(Input::get('sort'), $fields) ? Input::get('sort') : 'nome';
        $order = Input::get('order') === 'desc' ? 'desc' : 'asc';

        $clientes = $this->cliente->orderBy($sort, $order);

        if(Input::has('nome')) {
            $clientes = $clientes->where('nome', 'LIKE', "%". Input::get('nome') ."%");
            $cliente = '&cliente='. Input::get('nome');
        }

        $clientes = $clientes->paginate(15);

        $pagination = $clientes->appends(array(
            'nome' => Input::get('nome'),
            'sort' => Input::get('sort'),
            'order' => Input::get('order')
        ))->links();

        return View::make('clientes.index')
            ->with(array(
                'nome' => Input::get('nome'),
                'clientes' => $clientes,
                'pagination' => $pagination,
                'str' => '&order='.(Input::get('order') == 'asc' || null ? 'desc' : 'asc') . $nome
        ));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('clientes.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
        $validator = $this->cliente->validate($input);

        if($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator)
                ->with('danger', Lang::get('messages.MSG001'));
        } else {
            $this->cliente->create($input);

            return Redirect::to('cliente')
                ->with('success', Lang::get('messages.MSG002'));
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try {
            $cliente = $this->cliente->findOrFail($id);
            return View::make('clientes.edit', compact('cliente'));
        } catch(ModelNotFoundException $e) {
            return Redirect::to('cliente')
                ->with('danger', Lang::get('messages.MSG003'));
        }
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
        $validator = $this->cliente->validate($input);

        if($validator->fails()){
            return Redirect::back()
                ->withInput()
                ->withErrors($validator)
                ->with('danger', Lang::get('messages.MSG004'));
        } else {
            $this->cliente->find($id)->update($input);

            return Redirect::to('cliente')
                ->with('success', Lang::get('messages.MSG005'));
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try {
            $this->cliente->find($id)->delete();
            return Redirect::to('cliente')
                ->with('success', Lang::get('messages.MSG006'));
        } catch (Exception $e) {
            return Redirect::to('cliente')
                ->with('warning', Lang::get('messages.MSG007'));
        }	
	}


}
