<?php

class MarcaController extends BaseController {

	protected $marca;

    public function __construct(Marca $marca)
    {
        parent::__construct();
        $this->marca = $marca;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$marca = null;

        $fields = array('marca', 'placa', 'cor', 'ano', 'valor');
        $sort = in_array(Input::get('sort'), $fields) ? Input::get('sort') : 'marca';
        $order = Input::get('order') === 'desc' ? 'desc' : 'asc';

        $marcas = $this->marca->orderBy($sort, $order);

        if(Input::has('marca')) {
            $marcas = $marcas->where('marca', 'LIKE', "%". Input::get('marca') ."%");
            $marca = '&marca='. Input::get('marca');
        }

        $marcas = $marcas->paginate(15);

        $pagination = $marcas->appends(array(
            'marca' => Input::get('marca'),
            'sort' => Input::get('sort'),
            'order' => Input::get('order')
        ))->links();

        return View::make('marcas.index')
            ->with(array(
                'marca' => Input::get('marca'),
                'marcas' => $marcas,
                'pagination' => $pagination,
                'str' => '&order='.(Input::get('order') == 'asc' || null ? 'desc' : 'asc') . $marca
        ));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('marcas.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
        $validator = $this->marca->validate($input);

        if($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator)
                ->with('danger', Lang::get('messages.MSG001'));
        } else {
            $this->marca->create($input);

            return Redirect::to('marca')
                ->with('success', Lang::get('messages.MSG002'));
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try {
            $marca = $this->marca->findOrFail($id);
            return View::make('marcas.show', compact('marca'));
        } catch(ModelNotFoundException $e) {
            return Redirect::to('marca')
                ->with('danger', Lang::get('messages.MSG003'));
        }
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try {
            $marca = $this->marca->findOrFail($id);
            return View::make('marcas.edit', compact('marca'));
        } catch(ModelNotFoundException $e) {
            return Redirect::to('marca')
                ->with('danger', Lang::get('messages.MSG003'));
        }
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
        $validator = $this->marca->validate($input);

        if($validator->fails()){
            return Redirect::back()
                ->withInput()
                ->withErrors($validator)
                ->with('danger', Lang::get('messages.MSG004'));
        } else {
            $this->marca->find($id)->update($input);

            return Redirect::to('marca')
                ->with('success', Lang::get('messages.MSG005'));
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try {
            $this->marca->find($id)->delete();
            return Redirect::to('marca')
                ->with('success', Lang::get('messages.MSG006'));
        } catch (Exception $e) {
            return Redirect::to('marca')
                ->with('warning', Lang::get('messages.MSG007'));
        }	
    }


}
