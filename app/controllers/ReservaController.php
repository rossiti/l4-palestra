<?php

class ReservaController extends \BaseController {

	protected $reserva;

    public function __construct(Reserva $reserva)
    {
        parent::__construct();
        $this->reserva = $reserva;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$nome = $placa = null;

        $fields = array('nome', 'modelo', 'placa', 'reservas.created_at');
        $sort = in_array(Input::get('sort'), $fields) ? Input::get('sort') : 'nome';
        $order = Input::get('order') === 'desc' ? 'desc' : 'asc';

        $reservas = $this->reserva
        					->join('clientes', 'clientes.id', '=', 'reservas.cliente_id')
        					->join('veiculos', 'veiculos.id', '=', 'reservas.veiculo_id')
        					->select('clientes.nome', 'veiculos.modelo', 'veiculos.placa', 'reservas.*')
        					->orderBy($sort, $order);

        if(Input::has('nome')) {
            $reservas = $reservas->where('clientes.nome', 'LIKE', "%". Input::get('nome') ."%");
            $reserva = '&nome='. Input::get('nome');
        }

        if(Input::has('placa')) {
            $reservas = $reservas->where('veiculos.placa', 'LIKE', "%". Input::get('placa') ."%");
            $reserva = '&placa='. Input::get('placa');
        }

        $reservas = $reservas->paginate(15);

        $pagination = $reservas->appends(array(
            'nome' => Input::get('nome'),
            'placa' => Input::get('placa'),
            'sort' => Input::get('sort'),
            'order' => Input::get('order')
        ))->links();

        return View::make('reservas.index')
            ->with(array(
                'nome' => Input::get('nome'),
                'placa' => Input::get('placa'),
                'reservas' => $reservas,
                'pagination' => $pagination,
                'str' => '&order='.(Input::get('order') == 'asc' || null ? 'desc' : 'asc') . $nome . $placa
        ));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('reservas.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
        $validator = $this->reserva->validate($input);

        if($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator)
                ->with('danger', Lang::get('messages.MSG001'));
        } else {

            $this->reserva->create($input);

            return Redirect::to('reserva')
                ->with('success', Lang::get('messages.MSG002'));
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try {
            $this->reserva->find($id)->delete();
            return Redirect::to('reserva')
                ->with('success', Lang::get('messages.MSG006'));
        } catch (Exception $e) {
            return Redirect::to('reserva')
                ->with('warning', Lang::get('messages.MSG017'));
        }	
	}


}
